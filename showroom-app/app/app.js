import React,{Component} from 'react';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import OneSignal from 'react-native-onesignal';
import reducers from './reducers';
import {AppRegistry, Text,View} from 'react-native';
import Navigation from './util/navigation';

//cria state da aplicação
const store = createStore(reducers);

export default class App extends Component {
    componentDidMount() {
        OneSignal.configure({});
    }
  render() {
    return (
        <Provider store={store}>
            <Navigation />
        </Provider>
    );
  }
}
AppRegistry.registerComponent('ShowroomApp', () => App);