export const labels = {
    updateStateButton: "Atualizar estado",
    meetOurShowroom: "Conheça nosso Showroom! \n",
    approachAVehicle: "Aproxime-se de um veículo",
    loginPageMessage: "Efetue o login para utilizar o aplicativo",
    iAmVendor: "Sou vendendor",
    iAmUser: "Sou usuário",
    iAmClient: "Sou cliente",
    logoutButtonLabel: 'Sair',
    no:'não',
    yes:'sim',
    yourLogin:'Informe sua matrícula',
    yourPassword:'Informe sua senha',
    enter: 'Entrar'
};

export const messages = {
    findingVehicles: "Verificando veículos próximos...",
    errorGetBeaconPosition: "Erro na verificação",
    beaconNotFound: "Nenhum veículo encontrado!",
    welcomeMessage: "Olá, ",
    logout: "Efetuando Logout...",
    backgroundBeacon : "Existe um veículo próximo!",
    bluetoothDisabled: "É necessário ativar o bluetooth para a execução desta aplicação. Deseja ativar agora?",
    bluetoothDisabledTitle: "Habilitar Bluetooth",
    noInternet: "Você está sem internet",
    serverError: "Erro no servidor",
    userPassIncorrect: "Usuário ou senha incorretos!",
    requiredFieldsNull: "Existem campos obrigatórios não preenchidos!",
    thankYou: "Obrigado!"
};

export const titles = {
    lobby: "SHOWROOM APP TCC",
    login: "Login"
};

export const toastTypes = {
    INFO: "INFO",
    SUCCESS: "SUCCESS",
    ERROR:"ERROR"
};

export const userInfoDefault = {
    id: '',
    name: '',
    givenName: '',
    familyName: '',
    email: '',
    photo: '',
    idToken: '',
    serverAuthCode: '',
    scopes: '',
    accessToken: '',
};
