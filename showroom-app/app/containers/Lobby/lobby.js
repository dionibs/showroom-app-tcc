import React, {Component} from 'react';
import {View, Text,Image,StyleSheet,AppState,Dimensions, Alert} from 'react-native';
import {styles} from "../../styles/styles";
import {labels, messages, titles, toastTypes} from "../../constants";
import {toastTimeout, values, services, requestTypes, icons, appState, timeoutClientLimit} from "../../config/settings";
import BackgroundJob from 'react-native-background-job';
import PushNotification from 'react-native-push-notification';
import {connect} from "react-redux";
import {bindActionCreators} from 'redux';
import { getBeaconPosition } from '../../util/beacon/beaconDetection';
import ToastComponent from "../../components/Toasts/toastComponent";
import {showModalInfo, hideModalInfo, updateUserInfo, alterVisibilityLikes,setProfileVendor} from "../../actions";
import InfoCarModal from "../../components/Modals/infoCarModal";
import {setupGoogleSignin} from "../../util/loginTools";
import LogoutBar from "../../components/CustomComponents/logoutBar";
import { fetchCall } from '../../util/httpRequest';
import {routes} from '../../config/routes';
const backgroundJobKey = "backgroundJobKey";
const IMEI = require('react-native-imei');
import DeviceInfo from 'react-native-device-info';
import Icon from "react-native-vector-icons/SimpleLineIcons";
import {BluetoothStatus} from "react-native-bluetooth-status";

var waitingForLogin = false;

class Lobby extends Component{

    static navigationOptions =  ({ navigation }) => {
        const { state } = navigation;
        const { hiddenLabel } = "params" in state && state.params;
        var {height, width} = Dimensions.get('window')
        return ({
            headerTitle: <Image style={{height: height * 0.06, width: width * 0.2,  marginLeft: 10}} source={require('../../images/brand-logo.png')}/>,
            headerRight: hiddenLabel && hiddenLabel()
        });

    };

    state = {
        appState: AppState.currentState,
        bluetoothId: '',
        macAddress: '',
        deviceName: '',
        timeoutClient: 0,
        checkLogin: true,
        alertClient : "",
        visitedCarId : "",
        showBottomsMeetCustomer : true,
        isBusy : false,
        verifyBluetoothAlert : false
    };

    componentDidUpdate() {
        if (this.state.checkLogin === true) {
            this.checkLogin();
        }
    }

    componentDidMount() {
        AppState.addEventListener('change', this._handleAppStateChange);

        DeviceInfo.getMACAddress().then(mac => {
            this.setState({
                bluetoothId: IMEI.getImei(),
                macAddress: mac
            });
        }).catch((err) => {
            this.setState({
                bluetoothId: '-',
                macAddress: '-'
            });
            log(err.message);
        });

        this.checkLogin();

        this.startBackground();
    }

    componentWillUnmount() {
        AppState.removeEventListener('change', this.handleAppStateChange);
    };

    checkLogin = () => {
        if(this.props.isVendor){
            this.showWelcomeLabel(this.props.userInfo);
            this.setState({checkLogin : false});
        }else{
            let user = setupGoogleSignin();
            user.then((userInfo) => {
                if (userInfo != null) {
                    this.props.updateUserInfo(userInfo);
                    this.showWelcomeLabel(userInfo);
                    this.setState({checkLogin : false});
                    this.verifyBluetooth();
                } else {
                    if (waitingForLogin == false) {
                        this.props.navigation.navigate(routes.LoginPage);
                        waitingForLogin = true;
                    }
                }
            }).catch((err) => {
                log(err.message);
            });
        }

    };

    verifyBluetooth = () =>{
        if(this.state.verifyBluetoothAlert == false){
            this.setState({verifyBluetoothAlert : true});
            BluetoothStatus.state().
            then((isEnabled) => {
                if (!isEnabled) {
                    throw(messages.bluetoothDisabled);
                }
            }).catch((error) => {
                if (error.toString() === messages.bluetoothDisabled) {
                    Alert.alert(
                        messages.bluetoothDisabledTitle,
                        messages.bluetoothDisabled,
                        [
                            {text: labels.no, onPress: (() => {})},
                            {text: labels.yes, onPress: (() => {BluetoothStatus.enable()})}
                        ]
                    );
                } else {
                    Alert.alert(error.toString());
                }
            });
        }
    };

    performLogout = () => {
        this.refs.toast.showToast(toastTypes.INFO, messages.logout, toastTimeout.INFO);
        this.setState({checkLogin : true});
        waitingForLogin = false;
        if(this.props.isVendor) {
            this.props.navigation.navigate(routes.LoginPage);
            waitingForLogin = true;
            this.props.setProfileVendor(false);
            this.setState({showBottomsMeetCustomer : false});
            this.setState({alertClient : ""});
            this.logoffVendor();
        }else{
            this.checkLogin();
        }
    };

    logoffVendor = () => {
        let params = {
            idVendor : this.props.userInfo._id
        };

        fetchCall(services.logoffVendor, requestTypes.POST, ()=>{}, this.nearestCarError, params);
    };

    startBackground = () => {

        BackgroundJob.register({
            jobKey: backgroundJobKey,
            job: () => {
                if(this.props.isVendor){
                    this.vendorBackground();
                }else{
                    this.beaconBackground();
                }
            }
        });

        BackgroundJob.schedule({
            jobKey: backgroundJobKey,
            period: values.backgroundPeriod,
            exact: true,
            override: false,
            allowExecutionInForeground: true
        });
    };

    showWelcomeLabel = (userInfo, contextHome) => {
        if(!contextHome)
            contextHome = this;
        const hiddenLabel = (<LogoutBar userName={userInfo.name} isVendor={this.props.isVendor} performLogoutAction={this.performLogout}/>);
        contextHome.props.navigation.setParams({hiddenLabel: () => hiddenLabel});
    };

    _handleAppStateChange = (nextAppState) => {
        this.setState({appState: nextAppState});
    };

    getCarData = (coords) => {
        this.setState({timeoutClient: this.state.timeoutClient += (values.backgroundPeriod/1000)});
        let params = {
            bid : this.state.bluetoothId,
            mac : this.state.macAddress,
            beaconList : coords,
            timeoutClient : this.state.timeoutClient,
            userName : this.props.userInfo.name
        };
       fetchCall(services.nearestCar, requestTypes.POST, this.nearestCarSuccess, this.nearestCarError, params);
    };

    nearestCarSuccess = (response) => {
        if(response.status == 200) {
            if(this.state.timeoutClient > timeoutClientLimit){
                this.props.alterVisibilityLikes(response.data.isFirstVisitToday);
                this.initTimeoutClient();
            }

            if (this.state.appState == appState.background) {
                PushNotification.localNotification({
                    message: messages.backgroundBeacon
                });
            } else {
                if(!(this.props.jsonDataInfo && this.props.jsonDataInfo.carData == response.data.url)){
                    //diferente
                    this.props.showModalInfo({carData: response.data.url});
                    this.props.alterVisibilityLikes(response.data.isFirstVisitToday);
                    this.initTimeoutClient();
                }
            }
        } else {
            this.props.hideModalInfo();
            this.initTimeoutClient();
        }
    };

    nearestCarError = (response) => {
        if(response.status >= 500) {
            this.refs.toast.showToast(toastTypes.ERROR,messages.serverError, toastTimeout.ERROR);
        } else if(response.status >= 400) {
            this.refs.toast.showToast(toastTypes.ERROR,messages.noInternet, toastTimeout.ERROR);
        } else {
            this.refs.toast.showToast(toastTypes.ERROR, messages.errorGetBeaconPosition + ' ' + response.status + ': ' + response.message, toastTimeout.ERROR);
        }
        if(response.status){
            this.props.hideModalInfo();
        }
    };

    initTimeoutClient = () => {
        this.setState({
            timeoutClient : 0
        });
    };

    beaconBackground = () => {
        if (this.state.checkLogin == false) {

        let position = getBeaconPosition();
        position
            .then((position) => {
                this.getCarData(position.coords);
            })
            .catch((error) => {
                console.log(error);
            });

        }
    };

    vendorBackground = () => {
        let params = {
            idVendor : this.props.userInfo._id
        };
        if (this.state.checkLogin == false) {
            fetchCall(services.firstClient, requestTypes.GET, this.firstClientSuccess, this.nearestCarError, params);
        }
    };

    getStringResponseLike = (likes) =>{
        if(likes == null){
            return "e ainda não opinou sobre o veículo"
        }else{
            return likes == true ? "e gostou do veículo" : "e não gostou do veículo";
        }
    };

    firstClientSuccess = (response) => {
        if (response.length && response[0].nameClient){
            let mensagem = "Cliente " + response[0].nameClient + " está observando o veículo " + response[0].car + " a algum tempo "
                + this.getStringResponseLike(response[0].likes) +"!";

            this.setState({alertClient : mensagem});
            this.setState({visitedCarId : response[0].visitedCarId});
            this.setState({showBottomsMeetCustomer : true});

            if (this.state.appState == appState.background) {
                PushNotification.localNotification({
                    message: mensagem
                });
            }
        }
    };

    meetCustomerFinish = () => {
        let params = {
            idVendor : this.props.userInfo._id
        };

        fetchCall(services.meetCustomerFinish, requestTypes.POST, this.meetCustomerFinishSuccess, this.nearestCarError, params);
    };

    meetCustomerFinishSuccess = (response) => {
        if(response.status == 200) {
            this.setState({isBusy : false});
            this.setState({alertClient : ""});
        }
    };

    meetCustomer = (accept) => {
        let params = {
            idVendor : this.props.userInfo._id,
            visitedCarId : this.state.visitedCarId,
            accept : accept
        };
        if(accept){
            this.setState({isBusy : true});
        }else{
            this.setState({alertClient : ""});
        }
        fetchCall(services.meetCustomer, requestTypes.POST, this.meetCustomerSuccess, this.nearestCarError, params);
    };

    meetCustomerSuccess = (response) => {
        if(response.status == 200) {
            this.setState({showBottomsMeetCustomer : false});
        }
    };

    viewClient = () => {
        return (
            <View style={[styles.container, style.container]}>
                <Image
                    style = {style.image}
                    source={require('../../images/brand-logo512.png')}
                />
                <Text style = {style.text}>{labels.meetOurShowroom}</Text>
                <Text style = {style.text2}>{labels.approachAVehicle}</Text>

                <InfoCarModal macUser={this.state.macAddress}/>

                <ToastComponent ref="toast"/>
                <Text style={style.footer} />

            </View>
        );
    };

    validShowBottomsMeetCustomer = () =>{
        if(this.state.alertClient && this.state.showBottomsMeetCustomer == true && this.state.isBusy == false){
            return (
                <View style={style.botoes}>
                    <Icon.Button style={{height:50,width:120}} name="check" backgroundColor="#3b5998" onPress={()=>{this.meetCustomer(true)}}>
                        Atender
                    </Icon.Button>
                    <Text/>
                    <Icon.Button style={{height:50,width:120}} name="close" backgroundColor="#900" onPress={()=>{this.meetCustomer(false)}}>
                        Não atender
                    </Icon.Button>
                </View>
            );
        }
        if( this.state.isBusy == true){
            return (
                <View style={style.botoes}>
                    <Icon.Button style={{height:50,width:200}} name="close" backgroundColor="#900" onPress={()=>{this.meetCustomerFinish()}}>
                        Finalizar atendimento!
                    </Icon.Button>
                </View>
            );
        }
    };

    viewVendor = () => {
        return (
            <View style={[styles.container, style.container]}>
                <Image
                    style = {style.image}
                    source={require('../../images/brand-logo512.png')}
                />

                <Text style = {style.text}>{this.state.alertClient}</Text>
                {
                    this.validShowBottomsMeetCustomer()
                }

                <ToastComponent ref="toast"/>
                <Text style={style.footer} />
            </View>
        );
    };

    render(){
        return (
        this.props.isVendor ?
            this.viewVendor()
            :
            this.viewClient()
        );
    }
}

const mapStateToProps = (state) =>{
    return {
        jsonDataInfo: state.reducerState.jsonDataInfo,
        showModal: state.reducerState.showModal,
        userInfo: state.reducerState.userInfo,
        isVendor: state.reducerState.isVendor
    };
};

const mapDispathToProps = (dispatch) => {
    return bindActionCreators({showModalInfo, hideModalInfo, updateUserInfo, alterVisibilityLikes,setProfileVendor}, dispatch);
};

export default connect(mapStateToProps, mapDispathToProps)(Lobby);

const style = StyleSheet.create({
    container:{
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        position: 'absolute',
        top: 50,
        fontSize: 18,
        textAlign: 'center',
        color: '#000',
        borderRadius: 20,
        margin: 10,
        padding: 10,
        width: '80%',
        height: '40%',
    },
    text2: {
        position: 'absolute',
        top: 330,
        fontSize: 28,
        textAlign: 'center',
        color: '#000',
        borderRadius: 20,
        margin: 10,
        padding: 10,
        width: '80%',
        height: '40%',
    },
    image: {
        position: 'absolute',
        top: 120,
        width: '51%',
        height: '35 %',
        opacity: 0.1,
    },
    footer:{
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor:'#EF3B5D'
    },
    botoes:{position:'absolute',flexDirection : 'row' ,justifyContent: 'space-between',height:50,width:250}
});