import React, {Component} from 'react';
import {connect} from "react-redux";
import {StyleSheet, Text, View, BackHandler, Image} from 'react-native';
import GoogleLoginButton from '../../components/Button/googleLoginButton';
import {bindActionCreators} from 'redux';
import {titles, labels} from "../../constants";
import {updateUserInfo,setProfileVendor} from "../../actions";
import LoginPageVendor from "../../components/CustomComponents/loginPageVendor";
import { Button } from 'react-native-material-ui';

class LoginPage extends Component {
    static navigationOptions = {
        title: titles.login,
        headerLeft: null
    };

    state ={
        isLoginVendor:false
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', function() {
            return true;
        });
    }

    componentWillUnmount() {
        BackHandler.addEventListener('hardwareBackPress', function() {
            return false;
        });
    }

    updateUserInfo = (userInfo) => {
        this.props.updateUserInfo(userInfo);
        this.props.setProfileVendor(false);
        this.props.navigation.goBack();
    };

    isUserVendor = () => {
        this.setState({
            isLoginVendor : true
        })
    };

    isUser = () => {
        this.setState({
            isLoginVendor : false
        })
    };


    render() {
        return (

       this.state.isLoginVendor ?
           <LoginPageVendor goBack={this.isUser} updateUserInfo={this.updateUserInfo}/>
           :
            <View style={styles.container}>
                <Image
                    style = {styles.image}
                    source={require('../../images/brand-logo512.png')}
                />
               <Text style={styles.title}>
                   {labels.loginPageMessage}
               </Text>

               <View style={styles.loginButton}><GoogleLoginButton login={true} updateUserInfo={this.updateUserInfo}/></View>

               <View style={styles.loginVendor}><Button accent text={labels.iAmVendor} onPress={this.isUserVendor}/></View>
           </View>

        );
    }
}

const mapStateToProps = (state) =>{
    return {
        userInfo: state.reducerState.userInfo,
    };
};

const mapDispathToProps = (dispatch) => {
    return bindActionCreators({updateUserInfo,setProfileVendor}, dispatch);
};

export default connect(mapStateToProps, mapDispathToProps)(LoginPage);


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F4F6FE',
    },
    title: {
        marginBottom:30,
        justifyContent: 'center',
        alignItems: 'center',
    },
    loginButton: {
        width: '80%',
        height: 60,
    },

    logoutButton: {
        position:'absolute',
        bottom:'10%',
        right:'10%',
        height: 60,
    },

    loginVendor: {
        top:'20%'
    },
    image: {
        position: 'absolute',
        top: 50,
        width: '51%',
        height: '35%',
    }

});

