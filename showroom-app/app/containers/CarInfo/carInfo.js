import React, {Component} from 'react';
import SquareImage from '../../components/CustomComponents/squareImage';
import CarTitle from '../../components/Labels/carTitle';
import InfoSubtitle from '../../components/Labels/infoSubtitle';
import {View, StyleSheet, FlatList, Dimensions,Text,ScrollView } from "react-native";
import ActionFooter from '../../components/Footers/actionFooter';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

class CarInfo extends Component {
    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);

        this.state = {
            techDataButtonActive: false,
            defaultItemsButtonActive: true,
        }
    }



    viewTechData = () => {
        if (!this.state.techDataButtonActive) {
            this.setState({
                techDataButtonActive: true,
                defaultItemsButtonActive: false,
            });
        }
    };
    viewDefaultItems = () => {
        if (this.state.defaultItemsButtonActive == false) {
            this.setState({
                techDataButtonActive: false,
                defaultItemsButtonActive: true,
            });
        }
    };

    render() {
        const {jsonDataInfo} = this.props;
        return (
            <View>
                <View style={myStyles.container}>
                    <View style={myStyles.image}>
                        <SquareImage idCar={jsonDataInfo.carData.id}/>
                    </View>

                    <View style={myStyles.title}>
                        <CarTitle title={jsonDataInfo.carData.description}/>
                    </View>

                    {
                        this.state.defaultItemsButtonActive ?
                            <View >
                                <View style={myStyles.subTitle}>
                                    <InfoSubtitle subTitle={"Itens de Série"}/>
                                </View>
                                <View style={myStyles.list}>
                                    <FlatList
                                        showsVerticalScrollIndicator={false}
                                        data={jsonDataInfo.carData.defaultItems}
                                        renderItem={({item}) =>

                                            <Text style={[stylesDefaultItems.title]}>
                                                {'\u2022 ' + item.description}
                                            </Text>
                                        }
                                    />
                                </View>
                            </View>

                            :
                            <View>
                                <View style={myStyles.subTitle}>
                                    <InfoSubtitle subTitle={"Ficha técnica"}/>
                                </View>
                                <ScrollView contentContainerStyle={{height:600, weight: Dimensions.get('window').width - 50}}
                                            showsVerticalScrollIndicator={false}>
                                    <View style={{height:600, position: 'relative'}}>
                                        <View style={{}}>
                                        <Text style={stylesDefaultItems.tableTitle}>
                                            Dimensões
                                        </Text>

                                        <FlatList
                                            data={jsonDataInfo.carData.techFile}
                                            renderItem={({item}) =>

                                                <Text style={[stylesDefaultItems.title]}>
                                                    {'\u2022 ' + item.name + " - "+item.value}
                                                </Text>
                                            }
                                        />
                                        </View>

                                        <View style={{}}>

                                        <Text style={stylesDefaultItems.tableTitle}>
                                            Motor
                                        </Text>
                                        <FlatList
                                            data={jsonDataInfo.carData.engine}
                                            renderItem={({item}) =>

                                                <Text style={[stylesDefaultItems.title]}>
                                                    {'\u2022 ' + item.name + " - "+item.value}
                                                </Text>
                                            }
                                        />
                                        </View>
                                    </View>
                                </ScrollView>
                            </View>
                    }

                    <View style={myStyles.footer}>
                        <ActionFooter
                            firstButtonActive={this.state.defaultItemsButtonActive}
                            secondButtonActive={this.state.techDataButtonActive}
                            firstButtonAction={this.viewDefaultItems}
                            secondButtonAction={this.viewTechData}
                            firstIconName={'list'}
                            secondIconName={'wrench'}
                        />
                    </View>

                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) =>{
    return {
        jsonDataInfo: state.reducerState.jsonDataInfo,
        teste: state.reducerState.teste
    };
};
const mapDispathToProps = (dispatch) => {
    return bindActionCreators({}, dispatch);
};

export default connect(mapStateToProps,mapDispathToProps)(CarInfo);

const myStyles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        alignItems:'center',
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height - 80,
    },
    title: {
        height:25,
    },
    subTitle: {
        height:25,
        alignItems:'center',
        marginBottom: 10
    },
    image: {
        backgroundColor:'transparent',
        width: Dimensions.get('window').width - 30,
        height:125,
        alignItems:'center',
    },
    list: {
        position:'relative',
        padding:20,
        paddingTop: 0,
        marginBottom:50,
        borderTopWidth: 0,
        borderWidth: 0,
        backgroundColor: 'transparent',
        width: Dimensions.get('window').width - 30,
        height: Dimensions.get('window').height - 69 - 60 - 125 - 25 - 25 - 1,
    },
    footer: {
        position:'absolute',
        bottom: 0,
        width: Dimensions.get('window').width,
        height:60,
    },
});

const stylesDefaultItems = StyleSheet.create({
    container: {
        backgroundColor: '#BCBEC77F',
    },
    text: {
        margin: 6,
    },
    border: {
        borderWidth: 10,
        borderColor: '#F4F6FE'
    },
    title: {
        margin: 5,
        marginLeft: 15,
        fontSize: 13,
        fontWeight: 'bold'
    },
    header: {
        backgroundColor:'#BCBEC7'
    },
    tableTitle: {
        marginTop: 0,
        marginBottom: 5,
        fontSize: 16,
        fontWeight: 'bold',
    }
});