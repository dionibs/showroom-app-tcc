import {actions} from "./actionTypes";

export const showModalInfo = value =>{
    return {
        type: actions.SHOW_MODAL_INFO,
        jsonDataInfo: value
    };
};

export const hideModalInfo = () =>{
    return {
        type: actions.HIDE_MODAL_INFO,
    };
};

export const updateUserInfo = (userInfo) =>{
    return {
        type: actions.UPDATE_USER_INFO,
        userInfo: userInfo,
    };
};

export const setProfileVendor = (isVendor) =>{
    return {
        type: actions.IS_VENDOR,
        isVendor: isVendor,
    };
};


export const updateCacheCarInfo = (cache) =>{
    return {
        type: actions.UPDATE_CACHE_CAR_INFO,
        cacheCarInfo: cache,
    };
};
export const updateUrlCarInfo = (url) =>{
    return {
        type: actions.UPDATE_URL_CAR_INFO,
        urlCarInfo: url,
    };
};
export const alterVisibilityLikes = (showLikes) =>{
    return {
        type: actions.UPDATE_VISIBILITY_LIKES,
        showLikes: showLikes
    };
};