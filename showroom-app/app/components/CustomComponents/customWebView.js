import React, {Component} from 'react';
import {View, StyleSheet, WebView, Text, AsyncStorage,Alertnom } from 'react-native';
import {
    clearCache,
    getCarInfoDate,
    getCarInfoHtml,
    setCarInfoDate,
    setCarInfoHtml
} from "../../util/cache/cacheControl";
import {maxCacheIntervalMillisec} from "../../config/settings";
import {updateUrlCarInfo,updateCacheCarInfo} from "../../actions";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

var _this;
class CustomWebView extends Component {
    constructor(props) {
        super(props);
        this.getCarInfo(this.props.jsonDataInfo.carData);
    }

    getCarInfo = (carURL) => {
        let carDate = getCarInfoDate(carURL);
        carDate.then((date) => {
            let dateNow = (new Date).getTime();
            if ((date == null) || ((dateNow - date) >= maxCacheIntervalMillisec)) {
                this.saveCarInfo(carURL);
            } else {
                this.loadCarInfoFromCache(carURL);
            }
        });
    };

    loadCarInfoFromCache = (carURL) => {
       getCarInfoHtml(carURL).then((htmlString) => {
            this.props.updateCacheCarInfo({html: htmlString, baseUrl: carURL});
            this.props.updateUrlCarInfo('');
        });
    };

    saveCarInfo = (carURL) => {
        this.props.updateUrlCarInfo(carURL);

        fetch(carURL).then((response) => {
            response.text().then((htmlString) => {
                setCarInfoHtml(carURL,htmlString);
                setCarInfoDate(carURL);
            });
        });
    };

    goBack = () => {
        this.refs.webView.goBack();
    };

    render() {
        this.getCarInfo(this.props.jsonDataInfo.carData);
        const {baseUrl,html,url} = this.props;

        let webSource = (url === '') ?
            {
                html: html,
                baseUrl: baseUrl,
            }
            :
            {
                uri: url,
            };
        return(
                <WebView
                    ref="webView"
                    style={styles.webView}
                    source={webSource}
                    domStorageEnabled={true}
                    onNavigationStateChange={this.props.onNavigationStateChange}
                />
        )
    }
}

const mapStateToProps = (state) =>{
    return {
        baseUrl: state.reducerState.baseUrlCacheCarInfo,
        html: state.reducerState.htmlCacheCarInfo,
        url: state.reducerState.urlCarInfo,
        jsonDataInfo: state.reducerState.jsonDataInfo,
    };
};

const mapDispathToProps = (dispatch) => {
    return bindActionCreators({updateCacheCarInfo,updateUrlCarInfo}, dispatch);
};

export default connect(mapStateToProps, mapDispathToProps)(CustomWebView);


const styles = StyleSheet.create({
    webView: {
        flex: 1,
    },
});