import React, {Component} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {messages} from "../../constants";
import GoogleLogoutButton from "../Button/googleLogoutButton";

export default class LogoutBar extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.text}>{messages.welcomeMessage}{this.props.userName}</Text>
                <View style={styles.logoutButton}><GoogleLogoutButton isVendor={this.props.isVendor} performLogoutAction={this.props.performLogoutAction}/></View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
   container: {
       flex: 1,
       alignItems:'center',
       flexDirection: 'row',
   },
    text: {
        marginRight: 10,
        fontSize: 16,
    },
    logoutButton: {

    }
});