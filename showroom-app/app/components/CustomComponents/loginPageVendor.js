import React, {Component} from 'react';
import {StyleSheet, View, Text, TextInput, Keyboard, Image, Dimensions} from "react-native";
import {labels, messages, toastTypes} from "../../constants";
import {fetchCall} from "../../util/httpRequest";
import {requestTypes, services, toastTimeout} from "../../config/settings";
import {bindActionCreators} from "redux";
import {setProfileVendor} from "../../actions";
import connect from "react-redux/es/connect/connect";
import ToastComponent from "../Toasts/toastComponent";
import { Button } from 'react-native-material-ui';

class LoginPageVendor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            login : "",
            password : ""
        };
    }

    onChangeLogin = (value) => {
        this.setState({login:value});
    };

    onChangePassword = (value) => {
        this.setState({password:value});
    };

    login = () => {
        Keyboard.dismiss();
        if(this.state.login && this.state.password){
            fetchCall(services.loginVendor, requestTypes.POST, this.loginSuccess, this.loginError, this.state);
        }else{
            this.refs.toast.showToast(toastTypes.ERROR, messages.requiredFieldsNull, toastTimeout.ERROR);
        }
    };

    loginSuccess = (response) =>{
        if(response.status == 200) {
            if(response.data == null){
                this.refs.toast.showToast(toastTypes.ERROR, messages.userPassIncorrect, toastTimeout.ERROR);
                return;
            }
            this.props.updateUserInfo(response.data);
            this.props.setProfileVendor(true);
        }
    };

    loginError = (error) =>{
        this.refs.toast.showToast(toastTypes.ERROR, error.response.data, toastTimeout.ERROR);
    };

    render () {
        var {height, width} = Dimensions.get('window')
        return (
            <View style={styles.container}>
                <Image
                    style={{height: height * 0.3, width: width * 0.5, top: height * -0.1, marginBottom: -50}}
                    source={require('../../images/brand-logo512.png')}
                />

                <TextInput style={styles.input} placeholder={labels.yourLogin} onChangeText={value => this.onChangeLogin(value)} value={this.state.login}  />
                <TextInput style={styles.input} secureTextEntry={true} placeholder={labels.yourPassword}  onChangeText={value => this.onChangePassword(value)} value={this.state.password}  />
                <View style={styles.loginVendor}><Button raised primary text={labels.enter} onPress={this.login}/></View>

                <View style={styles.loginUser}><Button accent text={labels.iAmClient} onPress={this.props.goBack}/></View>

                <ToastComponent ref="toast"/>
            </View>
        )
    }
}

const mapStateToProps = (state) =>{
    return {
        userInfo: state.reducerState.userInfo,
    };
};

const mapDispathToProps = (dispatch) => {
    return bindActionCreators({setProfileVendor}, dispatch);
};

export default connect(mapStateToProps, mapDispathToProps)(LoginPageVendor);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F4F6FE',
    },
    title: {
        marginBottom:30,
        justifyContent: 'center',
        alignItems: 'center',
    },
    loginUser: {
        top:'20%'
    },
    loginVendor: {
        top:'10%'
    },
    input:{
        width : '70%'
    },
    image: {
        position: 'absolute',
        top: 20,
        width: '51%',
        height: '30%'
    }
});
