import React, {Component} from 'react';
import {StyleSheet, View, Image} from "react-native";


import img1 from '../../images/1.png';
import img2 from '../../images/2.png';
import img3 from '../../images/3.png';
import img4 from '../../images/4.png';
import img5 from '../../images/5.png';
import img6 from '../../images/6.png';

export default class SquareImage extends Component {

    getCarImage = (carId) =>{

        switch (carId){
            case 1:
                return img1;
                break;
            case 2:
                return img2;
                break;
            case 3:
                return img3;
                break;
            case 4:
                return img4;
                break;
            case 5:
                return img5;
                break;
            case 6:
                return img6;
                break;
            default:
                return img1;

        }
    };

    render () {
        return (
                <Image
                    style={styles.image}
                    source={this.getCarImage(this.props.idCar)}
                />
        )
    }
}

const styles = StyleSheet.create({
    image: {
        flex: 1,
        aspectRatio: 1.5,
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        alignItems:'center',
        alignContent:'center',
        justifyContent:'center',
    },
});