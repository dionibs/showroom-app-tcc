import React, {Component} from 'react';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import {View, StyleSheet, Dimensions} from 'react-native';

export default class ActionFooter extends Component {
    render() {
        let colorActive = '#EF3B5D';
        let colorInactive = '#74767F';

        let colorFirstButton = (this.props.firstButtonActive) ? colorActive : colorInactive;
        let colorSecondButton = (this.props.secondButtonActive) ? colorActive : colorInactive;

        return(
            <View style={styles.footer}>
                <IconFontAwesome
                    name={this.props.firstIconName}
                    color={colorFirstButton}
                    size={30}
                    onPress={this.props.firstButtonAction}
                />
                <IconFontAwesome
                    name={this.props.secondIconName}
                    color={colorSecondButton}
                    size={30}
                    onPress={this.props.secondButtonAction}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    footer: {
        flexDirection: 'row',
        alignItems:'center',
        justifyContent:'space-around',
        backgroundColor:'#F4F6FE00',
        width: Dimensions.get('window').width,
        height:60,
    },

});