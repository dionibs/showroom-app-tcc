import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import Toast from 'react-native-easy-toast';
import {toastTypes} from "../../constants";

export default class ToastComponent extends Component{

    showToast = (type, message, duration) =>{
        switch (type){
            case toastTypes.SUCCESS:{
                this.refs.toastInfo.close;
                this.refs.toastError.close;
                this.refs.toastSuccess.show(message, duration);
                break;
            }
            case toastTypes.INFO:{
                this.refs.toastSuccess.close;
                this.refs.toastError.close;
                this.refs.toastInfo.show(message, duration);
                break;
            }
            case toastTypes.ERROR:{
                this.refs.toastInfo.close;
                this.refs.toastSuccess.close;
                this.refs.toastError.show(message, duration);
                break;
            }
            default:{
                this.refs.toastSuccess.close;
                this.refs.toastError.close;
                this.refs.toastInfo.show(message, duration);
                break;
            }
        }
    }

    render(){
        return[
                <Toast key="toastSuccess" ref="toastSuccess" style={styles.toastSuccess}/>,
                <Toast key="toastError" ref="toastError" style={styles.toastError}/>,
                <Toast key="toastInfo" ref="toastInfo" style={styles.toastInfo}/>
        ];
    }
}

const styles = StyleSheet.create({
    toastSuccess:{
        backgroundColor:'#00E675',
        paddingBottom: 0
    },
    toastError:{
        backgroundColor:'#EF3B5D',
        paddingBottom: 0
    },
    toastInfo:{
        backgroundColor:'#5C89DB',
        paddingBottom: 0
    }
});