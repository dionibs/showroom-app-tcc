import React, {Component} from 'react';
import {Modal, StyleSheet, Dimensions, WebView, Text, View, Image} from 'react-native';
import {connect} from "react-redux";
import {alterVisibilityLikes, hideModalInfo} from "../../actions";
import {bindActionCreators} from 'redux';
import CustomWebView from "../CustomComponents/customWebView";
import Icon from 'react-native-vector-icons/SimpleLineIcons'
import {appState, requestTypes, services, toastTimeout} from "../../config/settings";
import {fetchCall} from "../../util/httpRequest";
import {labels, messages, toastTypes} from "../../constants";
import ToastComponent from "../Toasts/toastComponent";


class InfoCarModal extends Component {

    state ={
        backButtonEnabled:true
    };

    backHandler = () => {
        if(this.state.backButtonEnabled) {
            this.refs.customWebView.goBack();
            return true;
        } else {
          this.props.hideModalInfo();
           return true;
        }
    };

    onNavigationStateChange = (navState) => {
        this.setState({
            backButtonEnabled: navState.canGoBack
        });
    };

    saveOpinionClient = (opinion) =>{
        let params = {
            dataCar : this.props.jsonDataInfo.carData,
            userName : this.props.userInfo.name,
            likes: opinion,
            acceptReceiveInfos: false,
            macUser : this.props.macUser
        };
        fetchCall(services.opinionClient, requestTypes.POST, this.opinionSuccess, this.opinionError, params);
    };

    opinionSuccess = (response) => {
        this.props.alterVisibilityLikes(false);
        if(response.status == 200) {
            this.refs.toast.showToast(toastTypes.SUCCESS,messages.thankYou, toastTimeout.SUCCESS);
        }else{
            this.refs.toast.showToast(toastTypes.ERROR, response.status + ': ' + response.message , toastTimeout.ERROR);
        }
    };

    opinionError = (response) => {
        this.props.alterVisibilityLikes(false);
        if(response.status >= 500) {
            this.refs.toast.showToast(toastTypes.ERROR,messages.serverError, toastTimeout.ERROR);
        } else if(response.status >= 400) {
            this.refs.toast.showToast(toastTypes.ERROR,messages.noInternet, toastTimeout.ERROR);
        } else {
            this.refs.toast.showToast(toastTypes.ERROR, response.status + ': ' + response.message, toastTimeout.ERROR);
        }
    };

    like = () =>{
        this.saveOpinionClient(true);
    };

    noLike = () =>{
        this.saveOpinionClient(false );
    };

    viewButtons = () => {
        return (

            this.props.showLikes  ?
                <View style={styles.botoes }>
                    <Icon.Button style={{height:50,width:120}} name="like" backgroundColor="#3b5998" onPress={this.like}>
                        Gostei
                    </Icon.Button>
                    <Text/>
                    <Icon.Button style={{height:50,width:120}} name="dislike" backgroundColor="#900" onPress={this.noLike}>
                        Não gostei
                    </Icon.Button>
                </View>
                :
                <View/>

        );
    };

    render() {
        const {jsonDataInfo, showModal} = this.props;

        return (
            <Modal
                style={{backgroundColor:'blue'}}
                animationType="slide"
                transparent={true}
                presentationStyle="fullScreen"
                visible={showModal}
                onRequestClose={() => {
                    this.backHandler();
                }} >


                <CustomWebView
                    ref="customWebView"
                    carURL={jsonDataInfo.carData}
                    onNavigationStateChange={this.onNavigationStateChange}
                />
                {this.viewButtons()}
                <ToastComponent ref="toast"/>
            </Modal>
        );
    }
}

const mapStateToProps = (state) =>{
    return {
        jsonDataInfo: state.reducerState.jsonDataInfo,
        showModal: state.reducerState.showModal,
        userInfo: state.reducerState.userInfo,
        showLikes: state.reducerState.showLikes
    };
};

const mapDispathToProps = (dispatch) => {
    return bindActionCreators({hideModalInfo,alterVisibilityLikes}, dispatch);
};

export default connect(mapStateToProps, mapDispathToProps)(InfoCarModal);


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#0000003F',
        width: Dimensions.get('window').width,
        height: '100%',
        top: '8%',
        left: '0%',
    },
    botoes:{position:'absolute',flexDirection : 'row' ,justifyContent: 'space-between', left:60 ,  bottom:0,zIndex:1,height:50,width:250}
});
