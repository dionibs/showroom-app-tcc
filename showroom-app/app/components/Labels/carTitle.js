import React, {Component} from 'react';
import {View, StyleSheet, Text} from 'react-native';

export default class CarTitle extends Component {
    render() {
        return (
            <Text style={styles.title}>
                {this.props.title}
            </Text>
        )
    }
}

const styles = StyleSheet.create({
    title: {
        color: '#40434C',
        alignContent:'center',
        alignItems:'center',
        justifyContent:'center',
        fontSize: 20,
        fontWeight: 'bold',
    }
});