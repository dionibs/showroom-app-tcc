import React, {Component} from 'react';
import {View, StyleSheet, Text} from 'react-native';

export default class InfoSubtitle extends Component {
    render() {
        return (
            <Text style={styles.subtitle}>
                {this.props.subTitle}
            </Text>
        )
    }
}

const styles = StyleSheet.create({
    subtitle: {
        color: '#40434C',
        alignContent:'center',
        alignItems:'center',
        justifyContent:'center',
        fontSize: 18,
        fontStyle: 'italic',

    }
});