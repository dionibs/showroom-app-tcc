import React, {Component} from 'react';
import {GoogleSignin, GoogleSigninButton} from 'react-native-google-signin';
import { StyleSheet, Text, View, TouchableOpacity,} from 'react-native';
import { googleWebClientId } from '../../config/settings';

export default class GoogleLoginButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: null
        };
    }

    componentDidMount() {
        this.setupGoogleSignin();
    }

    render() {
        if (!this.state.user) {
            return (
                <GoogleSigninButton
                    style={styles.button}
                    color={GoogleSigninButton.Color.Dark}
                    size={GoogleSigninButton.Size.Wide}
                    onPress={() => {
                        this.signIn();
                    }}
                />
            );
        } else {
            return(
                <View/>
            );
        }
    }

    async setupGoogleSignin() {
        try {
            await GoogleSignin.hasPlayServices({ autoResolve: true });
            await GoogleSignin.configure({
                webClientId: googleWebClientId,
                offlineAccess: false
            });

            const user = await GoogleSignin.currentUserAsync();
            this.setState({user: user});
        }
        catch(err) {
            console.log("Play services error", err.code, err.message);
        }
    }

    signIn = () => {
        GoogleSignin.signIn()
            .then((user) => {
                this.props.updateUserInfo(user);
                this.setState({user: user});
            })
            .catch((err) => {
                alert(err);
                console.log('WRONG SIGNIN', err);
            })
            .done();
    };

    signOut = () => {
        GoogleSignin.revokeAccess().then(() => GoogleSignin.signOut()).then(() => {
            this.setState({user: null});
        })
            .done();
    };
}

const styles = StyleSheet.create({
    button: {
        width: '100%',
        height: 60,
    }
});

