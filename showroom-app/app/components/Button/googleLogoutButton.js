import React, {Component} from 'react';
import {GoogleSignin, GoogleSigninButton} from 'react-native-google-signin';
import { StyleSheet, Text, View, TouchableOpacity,} from 'react-native';
import { googleWebClientId } from '../../config/settings';
import {labels} from "../../constants";

export default class GoogleLogoutButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: null
        };
    }

    render() {
        return (
            <TouchableOpacity
                style={styles.button}
                onPress={() => {this.signOut(); }}>
                    <Text style={styles.text}>{labels.logoutButtonLabel}</Text>
            </TouchableOpacity>
        );
    }

    signOut = () => {
        if(this.props.isVendor){
            this.props.performLogoutAction();
        }else{
            GoogleSignin.revokeAccess().then(() => GoogleSignin.signOut()).then(() => {
                this.setState({user: null});
                this.props.performLogoutAction();
            })
                .done();
        }
    };
}

const styles = StyleSheet.create({
    button: {
        padding: 5,
        alignItems:'center',
        marginRight: 10,
    },
    text: {
        position:'relative',
        bottom:0,
        fontSize: 18,
        fontStyle: 'italic',
        textDecorationLine:'underline',
    }
});

