import {DURATION} from "react-native-easy-toast";

export const values = {
    scanInterval: 3000,
    backgroundPeriod: 5000
};

export const configBeacon = {
    //identifier: 'Beacons_FCA',
    uuid: 'b9407f30-f5f8-466e-aff9-25556b57fe6d',
    //12336 - minor: 15221
};
export const toastTimeout={
    ERROR:2000,
    SUCCESS:2000,
    INFO:3000,
    FOREVER: DURATION.FOREVER,
};

export const googleWebClientId='549733108078-otfq9krmhgb6dvn1phv11hkb458kfhji.apps.googleusercontent.com';

export const endpoints = {
    endpointMaster : "https://showroom-back-end.herokuapp.com"
};

export const services = {
    nearestCar : "nearestCar",
    loginVendor :"vendor/login",
    logoffVendor :"vendor/logoff",
    firstClient :"vendor/firstClient",
    opinionClient :"opinionClient",
    meetCustomer :"vendor/meetCustomer",
    meetCustomerFinish :"vendor/meetCustomerFinish",
};

export const requestTypes = {
    POST: 'POST',
    GET: 'GET',
    PUT: 'PUT',
    DELETE: 'DELETE',
};

export const icons = {
};

export const appState = {
    background : "background",
    foreground : "foreground",
};

const maxCacheIntervalHours = 24;
export const maxCacheIntervalMillisec = maxCacheIntervalHours*3600000;
export const timeoutClientLimit = 60;