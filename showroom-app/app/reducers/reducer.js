import {actions} from "../actions/actionTypes";
import {userInfoDefault} from '../constants';
import {Alert} from 'react-native';

/**
 * Handles the information sent to the State
 * @param state
 * @param action
 * @returns {{newValue: string}}
 */

 const initialState = {
    newValue: 'Atualizado via Redux!',
    jsonDataInfo: {},
    showModal: false,
    userInfo: userInfoDefault,
    baseUrlCacheCarInfo: '',
    htmlCacheCarInfo: '',
    urlCarInfo: '',
    isVendor: false,
    showLikes: false
}
export const reducer = (state = initialState, action={}) => {

    switch (action.type){
        case actions.UPDATE_VALUE:
            return {
                ...state,
                newValue: action.newValue
            };
        case actions.SHOW_MODAL_INFO:
            return {
                ...state,
                showModal: isShowModal(action),
                jsonDataInfo: action.jsonDataInfo
            };
        case actions.HIDE_MODAL_INFO:
            return {
                ...state,
                showModal: false,
                jsonDataInfo: {}
            };
        case actions.UPDATE_USER_INFO:
            return {
                ...state,
                userInfo: action.userInfo,
            };
        case actions.IS_VENDOR:
            return {
                ...state,
                isVendor: action.isVendor,
            };
        case actions.UPDATE_CACHE_CAR_INFO:
            return {
                ...state,
                baseUrlCacheCarInfo: action.cacheCarInfo.baseUrl,
                htmlCacheCarInfo: action.cacheCarInfo.html
            };
        case actions.UPDATE_URL_CAR_INFO:
            return {
                ...state,
                urlCarInfo: action.urlCarInfo,
            };
        case actions.UPDATE_VISIBILITY_LIKES:
            return {
                ...state,
                showLikes: action.showLikes
            };
        default:
            return state;
    }
};

 const isShowModal = (action)=>{
     if(action.jsonDataInfo && action.jsonDataInfo.carData){
         return true;
     }
     return false;
}