import React from 'react';
import {endpoints, requestTypes} from "../config/settings";
import axios from "axios";

export const fetchCall = (service, method, responseSuccess, responseError, param = {}) => {
    let url =  endpoints.endpointMaster + '/' + service;
    if(method === requestTypes.GET ){
        return axios.get(url, {params: param})
            .then((response) => {
                if(!response.status == 200) throw Error(response.statusText);
                return response.data;
            })
            .then(responseSuccess)
            .catch(responseError);
    } else if(method === requestTypes.DELETE ){
        return axios.delete(url, {params: param})
            .then((response) => {
                if (response.status == 204) {
                    return response;
                } else
                    throw response;
            })
            .then(responseSuccess)
            .catch(responseError);
    }else {
        return axios({
            method: method,
            url: url,
            data: JSON.stringify(param),
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        })
            .then((response) => {
                if ((response.status == 200) || (response.status == 204)) {
                    return response;
                } else
                    throw response;
            })
            .then(responseSuccess)
            .catch(responseError);
    }
};