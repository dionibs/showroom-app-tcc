import {AsyncStorage} from 'react-native';

export const getCarInfoHtml = (carURL) => {
    return(AsyncStorage.getItem(carURL));
};

export const setCarInfoHtml = (carURL, carInfoHtml) => {
    return(AsyncStorage.setItem(carURL, carInfoHtml));
};

export const getCarInfoDate = (carURL) => {
    return(AsyncStorage.getItem(carURL+'date'));
};

export const setCarInfoDate = (carURL) => {
    let date = (new Date).getTime().toString();
    return(AsyncStorage.setItem(carURL+'date',date));
};

export const clearCache = () => {
    return(AsyncStorage.clear());
};