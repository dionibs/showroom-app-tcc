import React from 'react';
import {StackNavigator} from "react-navigation";
import Lobby from '../containers/Lobby/lobby';
import LoginPage from '../containers/LoginPage/loginPage';

const Navigation = StackNavigator({
        Lobby: {screen: Lobby},
        LoginPage: {screen: LoginPage},
},
    {
        navigationOptions: {
            headerStyle: {
                backgroundColor: 'transparent',
            },
            headerTintColor: '#000000',
            headerTitleStyle: {
                textAlign: 'center',alignSelf:'center',
            },
        },
    }
    );
export default Navigation;