import {BeaconDetect} from './beacon';
import {values} from "../../config/settings"

export const getBeaconPosition = () => {
    return new Promise((resolve, reject) => {BeaconDetect.getBeaconPosition(values.scanInterval)
        .then((coords) => {
            let position = {
                coords: coords,
                message: '',
            };
            resolve(position);
        })
        .catch((error) => {
            let position = {
                coords: [],
                message: error,
            };
            reject(position);
        })
    });
};