import { DeviceEventEmitter } from 'react-native';
import Kontakt from 'react-native-kontaktio';
import {messages} from "../../constants";
import {configBeacon} from "../../config/settings"

const {
    connect,
    restartScanning,
    setBeaconRegions,
} = Kontakt;

var beacons = [];
var msg = '';
var isStarted = false;

const region = {
    //identifier: configBeacon.identifier,
    uuid: configBeacon.uuid,
};

export const BeaconDetect = {

    getBeaconPosition (scanInterval = 5000) {

        if (!isStarted) {
            isStarted = true;
                DeviceEventEmitter.addListener(
                'beaconDidAppear',
                ({beacon: newBeacon, region}) => {
                    beacons = beacons.concat(newBeacon);
                    msg = msg + 'beaconDidAppear - ';
                }
            );

            DeviceEventEmitter.addListener(
                'beaconsDidUpdate',
                ({beacons, region}) => {
                    msg = msg + 'beaconsDidUpdate - ';
                }
            );

            DeviceEventEmitter.addListener(
                'beaconDidDisappear',
                ({beacon: lostBeacon, region}) => {
                    msg = msg + 'beaconDidDisappear - ';
                }
            );
        }

        return (new Promise((resolve, reject) => {
                try {
                    connect()
                        //.then(() => setBeaconRegions([region]))
                        .then(() => restartScanning());

                    setTimeout(() => {
                        let coords = [];
                        for (var i in beacons) {
                            let coord = {
                                id: beacons[i].uuid,
                                major: beacons[i].major,
                                minor: beacons[i].minor,
                                distance: parseFloat(beacons[i].accuracy).toFixed(2),
                            };
                            coords = coords.concat(coord);
                        }
                        beacons = [];
                        msg = '';
                        //
                       /* let coord = {
                            id: 123,
                            major: 29869,
                            minor: 1,
                            distance:1 ,
                        };
                        coords = coords.concat(coord);*/
                        //

                        if (coords.length > 0) {
                            resolve(coords);
                        } else {
                            reject(messages.beaconNotFound);
                        }
                    }, scanInterval);
                }
                catch (error) {
                    reject(error);
                }
            }
        ));
    }
};