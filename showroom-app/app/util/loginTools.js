import {googleWebClientId} from "../config/settings";
import {GoogleSignin} from "react-native-google-signin";

export const setupGoogleSignin= () => {
    return(new Promise((resolve, reject) => {
        try {
            GoogleSignin.hasPlayServices({autoResolve: true}).then(() => {
                GoogleSignin.configure({
                    webClientId: googleWebClientId,
                    offlineAccess: false
                }).then(() => {
                    GoogleSignin.currentUserAsync().then((userInfo) => {
                        resolve(userInfo);
                    })
                })
            })
        } catch (err) {
            reject(err.message);
        }
    }));
};
