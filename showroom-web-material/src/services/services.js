const urlBase = "http://localhost:3001";//"http://jsonplaceholder.typicode.com/users";

export const getCountTotalCarsVisited = async () => {
    const data = await (await fetch(urlBase + '/car/countTotalVisited')).json();
    return data;
};

export const getCountCarsVisited = async () => {
    const data = await (await fetch(urlBase + '/car/countVisited')).json();
    let objReturn = {
        labels: [],
        series: [[]]
    };
    data.map((car) => {
        objReturn.labels.push(car[0]);
        objReturn.series[0].push(car[1]);
    });
    return objReturn;
};

export const getCountLikesCars = async () => {
    const data = await (await fetch(urlBase + '/car/likes')).json();
    let objReturn = {
        labels: [],
        series: [[]]
    };
    data.map((car) => {
        objReturn.labels.push(car[0]);
        objReturn.series[0].push(car[1]);
    });
    return objReturn;
};

export const getCountDislikesCars = async () => {
    const data = await (await fetch(urlBase + '/car/dislikes')).json();
    let objReturn = {
        labels: [],
        series: [[]]
    };
    data.map((car) => {
        objReturn.labels.push(car[0]);
        objReturn.series[0].push(car[1]);
    });
    return objReturn;
};

export const getStatusVendor =  async () => {
    const data = await (await fetch(urlBase + '/vendor/status')).json();
    let arrayVendor = [];
    data.map((vendor) => {
        let item = [vendor.vendorName, vendor.numberCalls.toString(), vendor.busy == true ? "SIM":"NÃO", vendor.working == true ? "SIM":"NÃO"];
        arrayVendor.push(item);
    });
    return arrayVendor;
};

export const getMoreCarsLikes = async () => {
    const data = await (await fetch(urlBase + '/car/moreLike')).json();
    return data;
};

export const getMoreCarsDisLikes = async () => {
    const data = await (await fetch(urlBase + '/car/moreDisLike')).json();
    return data;
};