import React,{useState} from "react";
import ChartistGraph from "react-chartist";
import { makeStyles } from "@material-ui/core/styles";
import Icon from "@material-ui/core/Icon";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/Table.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardIcon from "components/Card/CardIcon.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";

import { bugs, website, server } from "variables/general.js";

import {
  dailySalesChart,
  emailsSubscriptionChart,
  completedTasksChart
} from "variables/charts.js";

import styles from "assets/jss/material-dashboard-react/views/dashboardStyle.js";
import {
  getCountCarsVisited,
  getCountDislikesCars,
  getCountLikesCars, getCountTotalCarsVisited, getMoreCarsDisLikes, getMoreCarsLikes,
  getStatusVendor
} from "../../services/services";

const useStyles = makeStyles(styles);


export default function Dashboard() {
  const [statusVendors, setStatusVendors] = useState([[]]);
  const [countLikes, setCountLikes] = useState({labels: [],series: [[]]});
  const [countDisLikes, setCountDisLikes] = useState({labels: [],series: [[]]});
  const [countCarsVisited, setCountCarsVisited] = useState({labels: [],series: [[]]});
  const [countTotalCarsVisited, setCountTotalCarsVisited] = useState(0);
  const [moreCarLikes, setMoreCarLikes] = useState(["",0]);
  const [moreCarDisLikes, setMoreCarDisLikes] = useState(["",0]);
  const [init, setInit] = useState(1);
  const classes = useStyles();

  const updateDashBoard = async () =>{
    setStatusVendors(await getStatusVendor());
    setCountLikes(await getCountLikesCars());
    setCountDisLikes(await getCountDislikesCars());
    setCountCarsVisited(await getCountCarsVisited());
    setCountTotalCarsVisited(await getCountTotalCarsVisited());
    setMoreCarLikes(await getMoreCarsLikes());
    setMoreCarDisLikes(await getMoreCarsDisLikes());
  };

  if(init == 1){
    setInit(0);
    console.log("init");
    updateDashBoard();
  }
  return (
      <div>
        <GridContainer>
          <GridItem xs={12} sm={6} md={3}>
            <Card>
              <CardHeader color="warning" stats icon>
                <CardIcon color="warning">
                  <Icon>person</Icon>
                </CardIcon>
                <p className={classes.cardCategory}>Quantidade de visitas</p>
                <h3 className={classes.cardTitle}>
                  {countTotalCarsVisited}
                </h3>
              </CardHeader>
              <CardFooter stats>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <Card>
              <CardHeader color="success" stats icon>
                <CardIcon color="success">
                  <Icon>directions_car</Icon>
                </CardIcon>
                <p className={classes.cardCategory}>Carro mais aprovado</p>
                <h4 className={classes.cardTitle}>{moreCarLikes[0]}</h4>
                <h3 className={classes.cardTitle}>{moreCarLikes[1]}</h3>
              </CardHeader>
              <CardFooter stats>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <Card>
              <CardHeader color="danger" stats icon>
                <CardIcon color="danger">
                  <Icon>directions_car</Icon>
                </CardIcon>
                <p className={classes.cardCategory}>Carro mais reprovado</p>
                <h4 className={classes.cardTitle}>{moreCarDisLikes[0]}</h4>
                <h3 className={classes.cardTitle}>{moreCarDisLikes[1]}</h3>
              </CardHeader>
              <CardFooter stats>
              </CardFooter>
            </Card>
          </GridItem>
        </GridContainer>
        <GridContainer>
          <GridItem xs={12} sm={12} md={4}>
            <Card chart>
              <CardHeader color="rose">
                <ChartistGraph
                    className="ct-chart"
                    data={countCarsVisited}
                    type="Line"
                    options={dailySalesChart.options}
                    listener={dailySalesChart.animation}
                />
              </CardHeader>
              <CardBody>
                <h4 className={classes.cardTitle}>Carros visitados</h4>
                <p className={classes.cardCategory}>
                </p>
              </CardBody>
              <CardFooter chart>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <Card chart>
              <CardHeader color="success">
                <ChartistGraph
                    className="ct-chart"
                    data={countLikes}
                    type="Line"
                    options={dailySalesChart.options}
                    listener={dailySalesChart.animation}
                />
              </CardHeader>
              <CardBody>
                <h4 className={classes.cardTitle}>Quantidade Likes Carros</h4>
                <p className={classes.cardCategory}>
                </p>
              </CardBody>
              <CardFooter chart>
              </CardFooter>
            </Card>
          </GridItem>

          <GridItem xs={12} sm={12} md={4}>
            <Card chart>
              <CardHeader color="danger">
                <ChartistGraph
                    className="ct-chart"
                    data={countDisLikes}
                    type="Line"
                    options={dailySalesChart.options}
                    listener={dailySalesChart.animation}
                />
              </CardHeader>
              <CardBody>
                <h4 className={classes.cardTitle}>Quantidade Dislikes Carros</h4>
                <p className={classes.cardCategory}>
                </p>
              </CardBody>
              <CardFooter chart>
              </CardFooter>
            </Card>
          </GridItem>
        </GridContainer>
        <GridContainer>
          <GridItem xs={12} sm={12} md={6}>
            <Card>
              <CardHeader color="success">
                <h4 className={classes.cardTitleWhite}>Situação Vendedores</h4>
                <p className={classes.cardCategoryWhite}>
                  Acompanhamento diário dos vendedores
                </p>
              </CardHeader>
              <CardBody>
                <Table
                    tableHeaderColor="warning"
                    tableHead={["Nome", "Quantidade Atendimentos", "Ocupado", "Trabalhando"]}
                    tableData={statusVendors}
                />
              </CardBody>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
  );
}
