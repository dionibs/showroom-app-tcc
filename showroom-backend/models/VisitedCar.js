import mongoose from 'mongoose';
const Schema = mongoose.Schema;

let dataSchema = new Schema({
    nameClient: {type: String, required: true},
    macDevice: {type: String, required: true},
    carName: {type: String, required: true},
    date: {type: Date, required: true},
    urlCar: {type: String, required: false},
    likes: {type: Boolean, required: false},
    acceptReceiveInfos: {type: Boolean, required: false},
    vendorId: {type: Schema.Types.ObjectId, required: false},
    vendorName: {type: String, required: false}
}, {collection: 'visitedCar'});

export const VisitedCar = mongoose.model('VisitedCar', dataSchema);