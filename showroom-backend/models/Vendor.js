import mongoose from 'mongoose';
const Schema = mongoose.Schema;

let dataSchema = new Schema({
    name: {type: String, required: true},
    login: {type: String, required: true},
    password: {type: String, required: true}
}, {collection: 'vendor'});

export const Vendor = mongoose.model('Vendor', dataSchema);