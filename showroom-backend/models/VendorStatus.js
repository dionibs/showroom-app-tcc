import mongoose from 'mongoose';
const Schema = mongoose.Schema;

let dataSchema = new Schema({
    working: {type: Boolean, required: false},
    busy: {type: Boolean, required: false},
    numberCalls: {type: Number, required: false},
    vendorId: {type: Schema.Types.ObjectId, required: false},
    vendorName: {type: String, required: false}
}, {collection: 'vendorStatus'});

export const VendorStatus = mongoose.model('VendorStatus', dataSchema);