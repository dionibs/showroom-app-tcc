import mongoose from 'mongoose';
const Schema = mongoose.Schema;

let dataSchema = new Schema({
    majorBeacon: {type: Number, required: true},
    name: {type: String, required: true},
    url: {type: String, required: false}
}, {collection: 'car'});

export const Car = mongoose.model('Car', dataSchema);