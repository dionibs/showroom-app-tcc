import {VisitedCar} from "../models/VisitedCar";
import {VendorStatus} from "../models/VendorStatus";

export const getFirstClient = async (req, res) =>{
    if(req.query == null || req.query.idVendor == null){
        res.status(500);
        res.json(`Parametros incorretos`);
    }else {
        let visitedCar = null;
        let vendorValid = await validVendorStatus(req.query.idVendor);

        if(vendorValid){
            let iniDate = new Date().setHours(0, 0, 0);
            VisitedCar.find({vendorId: {'$eq': null}, date: {'$gte': iniDate}}, function(err, data){
                if(data.length){
                    visitedCar = data[0];
                    updateVisitedCarWithVendor(visitedCar._id, vendorValid.vendorId, vendorValid.vendorName);
                    res.status(200);
                    res.json([{visitedCarId : visitedCar._id, nameClient : visitedCar.nameClient, car : visitedCar.carName, likes: visitedCar.likes}]);
                }else{
                    res.status(200);
                    res.json([]);
                }
            });
        }else{
            res.status(200);
            res.json([]);
        }
    }
};

export const getStatusVendor = async (req, res) =>{
    await VendorStatus.find(function(err, data){
        res.status(200);
        res.json(data);
    });
};

const updateVisitedCarWithVendor = (visitedCarId, vendorId, vendorName) =>{
    VisitedCar.updateOne({_id: visitedCarId}, {
        vendorId: vendorId,
        vendorName: vendorName
    }, function(err, affected, resp) {
        console.log(affected);
    });
    //vendedor como ocupado, naão conta como atendiemnto, somente pare nao receber dois clientes ao mesmo tempo
    VendorStatus.updateOne({vendorId: vendorId}, { busy: true }, function(err, affected, resp) {
        console.log(affected);
    });
};

const validVendorStatus = async (id) =>{
    let vendors = await VendorStatus.find({working: true, busy: false}).sort({numberCalls : 1});
    if(vendors.length){
        if(vendors[0].vendorId == id){
            return vendors[0];
        }
    }
};

export const meetCustomer = async (req, res) =>{
    if(req.body == null || req.body.idVendor == null || req.body.visitedCarId == null  || req.body.accept == null){
        res.status(500);
        res.json(`Parametros incorretos`);
    }else {
        if(req.body.accept == true){
            VendorStatus.updateOne({vendorId: req.body.idVendor}, { $inc: { numberCalls: 1 }, busy: true }, function(err, affected, resp) {
                console.log(affected);
            });
        }else{
            VisitedCar.updateOne({_id: req.body.visitedCarId }, {
                vendorId: null,
                vendorName: null
            }, function(err, affected, resp) {
                console.log(affected);
            });

            VendorStatus.updateOne({vendorId: req.body.idVendor}, { busy: false }, function(err, affected, resp) {
                console.log(affected);
            });
        }
        res.status(200);
        res.json([]);
    }
};

export const meetCustomerFinish = async (req, res) =>{
    if(req.body == null || req.body.idVendor == null){
        res.status(500);
        res.json(`Parametros incorretos`);
    }else {
        VendorStatus.updateOne({vendorId: req.body.idVendor}, { busy: false }, function(err, affected, resp) {
            console.log(affected);
        });
        res.status(200);
        res.json([]);
    }
};