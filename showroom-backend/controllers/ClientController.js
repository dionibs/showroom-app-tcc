import {VisitedCar} from "../models/VisitedCar";

export const addOpinionClient=(req, res) => {
    if(req.body == null || req.body.dataCar == null || req.body.userName == null || req.body.likes == null
        || req.body.macUser == null || req.body.acceptReceiveInfos == null){
        res.status(500);
        res.json(`Parametros incorretos`);
    }else {
        let date = new Date().setHours(0, 0, 0);
        VisitedCar.updateOne({macDevice: req.body.macUser, urlCar: req.body.dataCar, date: {'$gte': date}}, {
            likes: req.body.likes,
            acceptReceiveInfos: req.body.acceptReceiveInfos
        }, function(err, affected, resp) {
            console.log(affected);
            console.log(resp);
        });
        res.status(200);
        res.json({data:"ok"});
    }
};