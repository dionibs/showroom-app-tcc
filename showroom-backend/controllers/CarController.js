import {VisitedCar} from "../models/VisitedCar";
import {Car} from "../models/Car";

export const getCarByBeacons= async (req, res) => {
    console.log(req.body);
    if(req.body == null || req.body.beaconList == null){
        res.status(500);
        res.json(`Informe a beaconList`);
    }else {
        res.status(200);

        let car = await getCar(req.body.beaconList);
        if(car && car.carName){
            let retorno = await checkTimeoutClient(req.body, car);
            let isFirstVisitToday = false;
            if(retorno){
                isFirstVisitToday = retorno.length > 0 ? false : true;
            }

            res.json({url:car.url,isFirstVisitToday: isFirstVisitToday});
        }else{
            res.json({url:"",isFirstVisitToday: false});
        }

    }
};

const getCar = async (beaconList) => {
    let beacon = getNearestBeacon(beaconList);
    if (beacon && beacon.major) {
        let data =  await Car.find({majorBeacon: beacon.major});
        if (data.length) {
            return {carName: data[0].name, url: data[0].url};
        }
    }
};

const getNearestBeacon = (beaconList) => {
    let minDistance = 2.00;
    let beacon = null;
    for(let i = 0;  i < beaconList.length; i++){
        if (beaconList[i].distance < minDistance) {
            beacon = beaconList[i];
            minDistance = beaconList[i].distance;
        }
    }
    return beacon;
};

const checkTimeoutClient = (body, car) =>{
  if(body.timeoutClient > 60){
      return addVisitedCar(body, car);
  }
};

const addVisitedCar = (body,car) =>{
    let obj = {nameClient: body.userName, macDevice: body.mac, carName: car.carName,
        urlCar: car.url,likes: null, acceptReceiveInfos: null, date: new Date(),
        vendorId: null, vendorName: null};
    let visitedCar = new VisitedCar(obj);
    let iniDate = new Date().setHours(0, 0, 0);
    return  VisitedCar.find({macDevice: body.mac, urlCar: car.url, date: {'$gte': iniDate}}, function(err, data){
        if(!data.length){
            visitedCar.save();
        }
    });
};

export const getCountLikesCar = async (req, res) =>{
    return  await VisitedCar.find({likes: true }, function(err, data){
        let map = new Map();
        data.map((car)=>{
            if(map.get(car.carName) != null){
                let value = map.get(car.carName) + 1;
                map.set(car.carName,value);
            }else{
                map.set(car.carName,1);
            }
        });
        res.status(200);
        res.json(map);
    });
};

export const getCountDisLikesCar = async (req, res) =>{
    return  await VisitedCar.find({likes: false }, function(err, data){
        let map = new Map();
        data.map((car)=>{
            if(map.get(car.carName) != null){
                let value = map.get(car.carName) + 1;
                map.set(car.carName,value);
            }else{
                map.set(car.carName,1);
            }
        });
        res.status(200);
        res.json(map);
    });
};

export const getCountVisitedCar = async (req, res) =>{
    return  await VisitedCar.find({}, function(err, data){
        let map = new Map();
        data.map((car)=>{
            if(map.get(car.carName) != null){
                let value = map.get(car.carName) + 1;
                map.set(car.carName,value);
            }else{
                map.set(car.carName,1);
            }
        });
        res.status(200);
        res.json(map);
    });
};

export const getCountTotalVisitedCar = async (req, res) =>{
    return  await VisitedCar.find({}, function(err, data){
        res.status(200);
        res.json(data.length);
    });
};

export const getCarMoreLike = async (req, res) =>{
    return  await VisitedCar.find({likes: true }, function(err, data){
        let map = new Map();
        data.map((car)=>{
            if(map.get(car.carName) != null){
                let value = map.get(car.carName) + 1;
                map.set(car.carName,value);
            }else{
                map.set(car.carName,1);
            }
        });
         let mapAsc = new Map([...map.entries()].sort((a, b) => a[1] - b[1]));
        res.status(200);
        res.json(Array.from(mapAsc)[Array.from(mapAsc).length -1]);
    });
};

export const getCarMoreDisLike = async (req, res) =>{
    return  await VisitedCar.find({likes: false }, function(err, data){
        let map = new Map();
        data.map((car)=>{
            if(map.get(car.carName) != null){
                let value = map.get(car.carName) + 1;
                map.set(car.carName,value);
            }else{
                map.set(car.carName,1);
            }
        });
        let mapAsc = new Map([...map.entries()].sort((a, b) => a[1] - b[1]));
        res.status(200);
        res.json(Array.from(mapAsc)[Array.from(mapAsc).length -1]);
    });
};