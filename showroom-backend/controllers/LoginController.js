import {Vendor} from "../models/Vendor";
import {VendorStatus} from "../models/VendorStatus";

export const login=(req, res) => {
    console.log(req.body);
    if(req.body == null || nullOrEmpty(req.body.login) || nullOrEmpty(req.body.password)){
        res.status(500);
        res.json(`Informe todos os dados para login`);
    }else {
        Vendor.findOne({login:req.body.login, password: req.body.password}, function(err, vendor) {
            if (err) return console.error(err);
            if(vendor && vendor._id){
                VendorStatus.updateOne({vendorId: vendor._id}, { working: true, busy: false }, function(err, affected, resp) {
                    console.log(affected);
                });
            }

            res.status(200);
            res.json(vendor);
        });

    }
};

export const logoff = async (req, res) =>{
    if(req.body == null || req.body.idVendor == null){
        res.status(500);
        res.json(`Parametros incorretos`);
    }else {
        VendorStatus.updateOne({vendorId: req.body.idVendor}, { working: false, busy: false }, function(err, affected, resp) {
            console.log(affected);
        });
        res.status(200);
        res.json([]);
    }
};

export const newVendor=(req, res) => {
    if(req.body == null || nullOrEmpty(req.body.name) || nullOrEmpty(req.body.login) || nullOrEmpty(req.body.password)){
        res.status(500);
        res.json(`Informe todos os dados para criar o Vendedor`);
    }else {
        let vendor = new Vendor({name: req.body.name, login: req.body.login, password: req.body.password});
        vendor.save(function(err,vendor) {
            createVendorStatus(vendor.id,vendor.name);
        });
        res.status(200);
        res.json("OK");
    }
};

const createVendorStatus = (id, name) =>{
    let vendorStatus = new VendorStatus({working: false, busy: false, numberCalls: 0,vendorId:id,vendorName:name});
    vendorStatus.save();
};

const nullOrEmpty = (value) => {
    return value == null || value.length == 0;
};