import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import {
    getCarByBeacons, getCarMoreDisLike, getCarMoreLike,
    getCountDisLikesCar,
    getCountLikesCar,
    getCountTotalVisitedCar,
    getCountVisitedCar
} from './controllers/CarController';
import {login, logoff, newVendor} from "./controllers/LoginController";
import {addOpinionClient} from "./controllers/ClientController";
import {getFirstClient, getStatusVendor, meetCustomer, meetCustomerFinish} from "./controllers/VendorController";
var cors = require('cors');
var app = express();
app.use(cors())

app.use(bodyParser.json());

app.get('/', function (req, res) {
    res.send('Hello World!');
});

app.post('/nearestCar', getCarByBeacons);
app.post('/opinionClient', addOpinionClient);

app.post('/vendor', newVendor);
app.post('/vendor/login', login);
app.post('/vendor/logoff', logoff);
app.get('/vendor/firstClient', getFirstClient);
app.post('/vendor/meetCustomer', meetCustomer);
app.post('/vendor/meetCustomerFinish', meetCustomerFinish);
app.get('/vendor/status', getStatusVendor);

app.get('/car/likes', getCountLikesCar);
app.get('/car/dislikes', getCountDisLikesCar);
app.get('/car/countVisited', getCountVisitedCar);
app.get('/car/countTotalVisited', getCountTotalVisitedCar);
app.get('/car/moreLike', getCarMoreLike);
app.get('/car/moreDisLike', getCarMoreDisLike);

mongoose.connect('mongodb+srv://user_db:pass_db@cluster0-u9vr8.mongodb.net/showroomTCC?retryWrites=true&w=majority');
var Schema = mongoose.Schema;

var port = process.env.PORT || 3001;


app.listen(port, function () {
    console.log('App listening on port '+port);
});